export const constants = {
  events: {
    MESSAGE: 'message',
    NEW_USER_CONNECTED: 'newUserConnected',
    DISCONNECT_USER: 'disonnectUser',
    UPDATE_USERS: 'updateUsers',
  },
}
